<?php
/**
 * Created by PHP是世界上最好的语言
 * Description: GrpcController
 * @author fyk
 * @E-mail: 414734650@qq.com
 * @Date 2022/6/10
 * @Time 16:15
 */

namespace App\Controller;

class GrpcController
{
    public function hello()
    {
        // 这个client是协程安全的，可以复用
        $client = new \App\Grpc\HiClient('127.0.0.1:9503', [
            'credentials' => null,
        ]);

        $request = new \Grpc\HiUser();
        $request->setName('hyperf');
        $request->setSex(1);

        /**
         * @var \Grpc\HiReply $reply
         */
        list($reply, $status) = $client->sayHello($request);

        $message = $reply->getMessage();
        $user = $reply->getUser();

        var_dump(memory_get_usage(true));
        return $message;
    }
}