package service

import (
	"context"
	"goServer/pb"
)

type Hello struct{}

func (cls *Hello) HelloInfo(ctx context.Context, pkg *pb.HelloInfoRequest) (*pb.HelloInfoReply, error) {
	return &pb.HelloInfoReply{
		Name: pkg.Name,
	}, nil
}
